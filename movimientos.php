<?php
require "php/variables.php";
require "php/funciones.php";
require "clases/Sesion.php";
require "clases/dbMySQL.php";
require "clases/Usuarios.php";
require "clases/Cuentas.php";
require "clases/Categorias.php";
require "clases/Movimientos.php";
/****************
Leemos la sesión
*****************/
$sesion = new Sesion();
$usuario = $sesion->getUsuario();
$data = Usuarios::leeUsuario($usuario);
$id = $data["id"]; //identificador del usuario
/*****************
Lee movimientos
******************/
$movimiento = new Movimientos();
$movimientos_array = array();
$numRegistros = $movimiento->numRegistros($id);
/*****************************
Lee cuentas (realizamos pagos)
******************************/
$cuenta = new Cuentas();
$cuentas_array = array();
$cuentas_array = $cuenta->leeCuentasUsuario($id);
/******************/
require "php/paginaArriba.php";
/****************
Variables de trabajo
********************/
$idMovimiento = "";
$idCuenta= "";
$idCategoria= "";
$tipo = "";
$monto = 0;
$fecha = "";
$nota = "";
/****************
Modo de la página (CRUD o ABC)
S - Consulta (select)
A - Alta (insert)
B - Borrar (delete)
C - Cambiar (update)
D - Baja Definitiva
*****************/
if (isset($_GET["m"])) {
	$m = $_GET["m"];
} else {
	$m = "S";
}
/**************
Validacion
**************/
if (isset($_POST["cuenta"])) {
	$tipo = isset($_POST["tipo"])? $_POST["tipo"] : null;
	$categoria = isset($_POST["categoria"])? $_POST["categoria"] : null;
	$monto = isset($_POST["monto"])? $_POST["monto"] : null;
	$idCuenta = isset($_POST["cuenta"])? $_POST["cuenta"] : null;
	$fecha = isset($_POST["fecha"])? $_POST["fecha"] : null;
	$nota = isset($_POST["nota"])? $_POST["nota"] : null;
	$monto = limpiaNumero($monto);
	//
	if(!validaRequerido($tipo)) $msg[] = "1El campo de 'tipo' es requerido.";
	if(!validaRequerido($categoria)) $msg[] = "1El campo de 'categoría' es requerido.";
	if(!validaRequerido($idCuenta)) $msg[] = "1El campo de 'cuenta' es requerido.";
	if(!validaDecimal($monto)) $msg[] = "1El campo de 'monto' es incorrecto.";
	if($monto<=0) $msg[] = "1El campo de 'monto' no puede ser menor o igual a cero.";
	if(!validaFecha($fecha)) $msg[] = "1El campo de 'fecha' es incorrecto.";
	if (count($msg)==0) {
		$movimiento->altaMovimiento("",$id,"",$tipo, $categoria, $idCuenta, $monto, $fecha,$nota);
		$cuenta->actualizaSaldo($idCuenta,$tipo,$monto);
	} else {
		$m = "A";
	}
}
//Baja definitiva
if($m=="D"){
	$idMovimiento = $_GET["id"];
	//leemos los datos del registro
	$data = $movimiento->leerRegistro($idMovimiento);
	$idCuenta= $data[0]["cuenta"];
	$tipo = $data[0]["tipo"];
	$monto = $data[0]["monto"];
	//Borramos el movimiento
	$movimiento->borrarRegistro($idMovimiento);
	//Actualizamos el saldo
	$cuenta->actualizaSaldo($idCuenta,$tipo,$monto*-1);
	$m = "S";
}
//Consulta o baja (previa) del registro
if($m=="B"){
	$idMovimiento = $_GET["id"];
	$data = $movimiento->leerRegistro($idMovimiento);
	//
	$idCuenta= $data[0]["cuenta"];
	$idCategoria= $data[0]["categoria"];
	$tipo = $data[0]["tipo"];
	$monto = $data[0]["monto"];
	$fecha = $data[0]["fecha"];
	$nota = $data[0]["nota"];
	$cuentaNombre = $data[0]["cuentaNombre"];
	$categoriaNombre = $data[0]["categoriaNombre"];
} else if($m=="S"){
	$movimientos_array = $movimiento->leeMovimientosUsuario($id,$inicio,$TAMANO_PAGINA);
}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Control de Gastos | Movientos</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1"/>
	<link rel="shortcut icon" href="imagenes/favicon.ico">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
	<script>
		window.onload = function(){
			<?php if($m=="S"){ ?>
				document.getElementById("alta").onclick = function(){
					window.open("movimientos.php?m=A","_self");
				}
			<?php } 
			
			if($m=="C" || $m=="A"){ ?>
				document.getElementById("regresar").onclick = function(){
					window.open("movimientos.php","_self");
				}
			<?php } 
			
			if($m=="B"){ ?>
				document.getElementById("si").onclick = function(){
					var idMovimiento = <?php print $idMovimiento; ?>;
					window.open("movimientos.php?m=D&id="+idMovimiento,"_self");
				}
				document.getElementById("no").onclick = function(){
					window.open("movimientos.php","_self");
				}
			<?php } else  {?>

				document.getElementById("tipo").onchange = function() {
					var tipo = this.value;
					console.log(tipo);
					despliegaCategorias(tipo);
				}

			<?php } ?>
		}
		function cambiaPagina(p) {
			window.open("movimientos.php?p="+p,"_self");
		}
		function despliegaCategorias(tipo){
			if (tipo=="") return;
			var xmlhttp;
			var usuario = <?php print $id; ?>;
			if (window.XMLHttpRequest) {
				xmlhttp = new XMLHttpRequest();
			} else {
				//IE5 o 6
				xmlhttp = new ActiveXObject("Microsoft.HTMLHTTP");
			}
			xmlhttp.open("GET","php/leeCategorias.php?t="+tipo+"&usuario="+usuario,true);
			xmlhttp.send();
			xmlhttp.onreadystatechange = function() {
				//estado 4 y 200
				if (xmlhttp.readyState==4) {
					if (xmlhttp.status==200) {
						creaCombo(xmlhttp.responseXML);
					} else {
						alert("Error al leer las categorías ".xmlhttp.status);
					}
				}
			}
		}
		function creaCombo(objetoXML) {
			var a = objetoXML.documentElement.getElementsByTagName("categoria");
			var categoria_cb = document.getElementById("categoria");
			//Limpiar el combo
			while(categoria_cb.length) categoria_cb.remove(0);
			//
			var op = document.createElement("option");
			op.innerHTML= "--Selecciona una categoría--";
			op.setAttribute("value","0");
			categoria_cb.appendChild(op);
			//
			for (var i = 0; i < a.length; i++) {
				num = a[i].getElementsByTagName("id");
				numCategoria = num[0].firstChild.nodeValue;
				//
				cat = a[i].getElementsByTagName("nombre");
				nomCategoria = cat[0].firstChild.nodeValue;
				//
				//creamos la etiqueta <option>
				//
				var op = document.createElement("option");
				op.innerHTML= nomCategoria;
				op.setAttribute("value",numCategoria);
				categoria_cb.appendChild(op); 
			}
		}
	</script>
	<style>
	button{ cursor:pointer; }
	</style>
</head>
<body>
	<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
		<a href="inicio.php" class="navbar-brand">Gastos</a>
		<ul class="navbar-nav mr-auto mt-2 mt-lg-0">
			<li class="nav-item">
				<a href="inicio.php" class="nav-link">Resumen</a>
			</li>
			<li class="nav-item">
				<a href="categorias.php" class="nav-link">Categorías</a>
			</li>
			<li class="nav-item">
				<a href="cuentas.php" class="nav-link">Cuentas</a>
			</li>
			<li class="nav-item active">
				<a href="movimientos.php" class="nav-link">Movimientos</a>
			</li>
			<li class="nav-item">
				<a href="traspasos.php" class="nav-link">Traspasos</a>
			</li>
			<li class="nav-item">
				<a href="presupuesto.php" class="nav-link">Presupuesto</a>
			</li>
			<li class="nav-item">
				<a href="cxc.php" class="nav-link">CXC</a>
			</li>
			<li class="nav-item">
				<a href="admon.php" class="nav-link">Admon</a>
			</li>
		</ul>
		<ul class="navbar-nav">
			<li class="nav-item">
				<a href="salir.php" class="nav-link">Salir</a>
			</li>
		</ul>
	</nav>
	<div class="container-fluid text-center">
		<div class="row content">
			<div class="col-sm-2 sidevar">
				<?php if ($m=="S") { ?>
					<label for="alta"></label>
					<input type="button" name="alta" value="Dar de alta un movimiento" class="btn btn-info mt-4" role="button" id="alta">
				<?php } ?>
			</div>
			<div class="col-sm-9 text-center">
				<h2>Movimientos</h2>
				<?php if($m=="C" || $m=="A" || $m=="B") { 
					require "php/mensajes.php";
				?>
					<form action="movimientos.php" method="post">
						<div class="form-group">
							<label for="tipo">* Tipo de cuenta:</label><br>
							<?php
							if($m=="B"){
								print "<input class='form-control' type='text' id='tipo' name='tipo' value='".$tipo."' disabled/>";
							} else { ?>
							<select id="tipo" name="tipo" class="form-control">
								<option value="">Selecciona un tipo de cuenta</option>
								<option value="gasto">Gasto</option>
								<option value="ingreso">Ingreso</option>
							</select>
							<?php } ?>
						</div>
						<div class="form-group">
							<label for="categoria">* Categoria:</label><br>
							<?php 
							if($m=="B"){
								print "<input class='form-control' type='text' id='categoria' name='categoria' value='".$categoriaNombre."' disabled/>";
							} else { ?>
								<select id="categoria" name="categoria" class="form-control">
									<option value="">Selecciona una categoria</option>
								</select>
							<?php } ?>
						</div>
						<div class="form-group">
							<label for="cuenta">* Cuenta:</label><br>
							<select id="cuenta" name="cuenta" class="form-control" <?php print ($m=='B')?'disabled':""; ?>>
								<option value="">* Forma de pago</option>
								<?php
								for ($i=0; $i < count($cuentas_array); $i++) { 
									print "<option ";
									print ($cuentas_array[$i]["id"]==$idCuenta)?"selected":"";
									print " value='".$cuentas_array[$i]["id"]."'>";
									print $cuentas_array[$i]["cuenta"];
									print "</option>";
								}
								?>
							</select>
						</div>
						<div class="form-group">
							<label for="monto">* Monto:</label>
							<input type="text" name="monto" id="monto" required class="form-control" placeholder="Escribe el monto del movimiento" value="<?php print number_format($monto,2); ?>" <?php print ($m=='B')?'disabled':""; ?>/>
						</div>
						<div class="form-group">
							<label for="fecha">* Fecha:</label>
							<input type="date" name="fecha" id="fecha" required class="form-control" placeholder="AAAA-MM-DD" value="<?php print $fecha; ?>" <?php print ($m=='B')?'disabled':""; ?>/>
						</div>
						<div class="form-group">
							<label for="nota">Nota:</label>
							<input type="text" name="nota" id="nota" class="form-control" placeholder="Escribe una nota" value="<?php print $nota; ?>" <?php print ($m=='B')?'disabled':""; ?>/>
						</div>
						<input type="hidden" id="idCuenta" name="idCuenta" value="<?php print $idCuenta; ?>">
						<?php if($m=="C" || $m=="A") { ?>
						<div class="form-group">
							<label for="enviar"></label>
							<input type="submit" name="enviar" id="enviar" class="btn btn-success" value="Enviar datos"/>

							<label for="regresar"></label>
							<input type="button" name="regresar" id="regresar" class="btn btn-info" value="Regresar" role="button"/>
						</div>
						<?php } else if($m=="B"){?>
						<div class="alert alert-danger">
							<p><b>Advertencia:</b> Una vez borrado el registro, no se podrá recuperar.</p>
							<p>La eliminición de un saldo modificará sus saldos.</p>
							<p>¿Desea borrar el registro?</p>
							<label for="si"></label>
							<input type="button" name="si" id="si" class="btn btn-danger" value="Si"/>

							<label for="No"></label>
							<input type="button" name="no" id="no" class="btn" value="No" role="button"/>
						</div>
						<?php } ?>
					</form>
				<?php
				}
				if($m=="S"){
					print "<table class='table table-striped' width='100%'>";
					print "<tr>";
					print "<th>Fecha</th>";
					print "<th>Cuenta</th>";
					print "<th>Categoria</th>";
					print "<th>Cargo</th>";
					print "<th>Abono</th>";
					print "<th>Nota</th>";
					print "<th>Borrar</th>";
					print "</tr>";
					for ($i=0; $i < count($movimientos_array); $i++) {
						$monto = $movimientos_array[$i]["monto"];
						print "<tr>";
						print "<td>".$movimientos_array[$i]["fecha"]."</td>";
						print "<td>".$movimientos_array[$i]["cuentaNombre"]."</td>";
						print "<td>".$movimientos_array[$i]["categoriaNombre"]."</td>";
						if ($movimientos_array[$i]["tipo"]=="gasto") {
							print "<td>".number_format($monto,2)."</td>";
							print "<td>-</td>";
						} else {
							print "<td>-</td>";
							print "<td>".number_format($monto,2)."</td>";
						}
						print "<td>".$movimientos_array[$i]["nota"]."</td>";
						print "<td><a class='btn btn-danger' href='movimientos.php?m=B&id=".$movimientos_array[$i]["id"]."'>Borrar</a></td>";
						print "</tr>";
					}
					print "</table>";
					require "php/paginaBaja.php";
				}
				
				?>
			</div>
			<div class="col-sm-1 sidevar"></div>
		</div>
	</div>
</body>
</html>