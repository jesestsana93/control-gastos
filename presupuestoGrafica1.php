<?php
require "php/variables.php";
require "php/funciones.php";
require "clases/Sesion.php";
require "clases/dbMySQL.php";
require "clases/Usuarios.php";
require "clases/Categorias.php";
/****************
Leemos la sesión
*****************/
$sesion = new Sesion();
$usuario = $sesion->getUsuario();
$data = Usuarios::leeUsuario($usuario);
$id = $data["id"]; //identificador del usuario
/*****************
Lee categorias
******************/
$categoria = new Categorias();
$categorias_array = array();
$numRegistros = $categoria->numRegistros($id);
if($numRegistros==0){
	array_push($msg,"1Error no hay categorías");
} else {
	$categorias_array = $categoria->leeCategoriasUsuario($id,0,$numRegistros);
}
/**********
Variables
**********/
$tot1 = 0;
$tot2 = 0;
$tot3 = 0;
$ejercido = 0;
$restante = 0;
/****************/
?>
<!DOCTYPE html>
<html>
<head>
	<title>Control de Gastos | Presupuesto Gráfica de pie</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1"/>
	<link rel="shortcut icon" href="imagenes/favicon.ico">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
	<!--Cargar AJAX API-->
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
	<script>
		window.onload = function(){
			document.getElementById("tabla").onclick = function() {
				window.open("presupuesto.php","_self");
			}
		}
	</script>
    <script>

      // Carga el API de visualización y el paquete corechart
      google.charts.load('current', {'packages':['corechart']});

      // Define la función callback para crear la gráfica
      google.charts.setOnLoadCallback(grafica);

      // Función para poblar la gráfica
      // iniciar el gráfico, pasa los datos y los dibuja
      function grafica() {

        // Crea la gráfica
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Navegador');
        data.addColumn('number', 'Porciento');
        data.addRows([
          
        <?php
          $n = count($categorias_array);
          for ($i = 0; $i < $n; $i++) {
          	if($categorias_array[$i]["tipo"]=="gasto"){
          		print "['".$categorias_array[$i]["categoria"]."', ".$categorias_array[$i]["presupuesto"]."]";
            	if(($i+1)<$n) print ",";
          	}
          }
        ?>
         
        ]);

        // Opciones de la gráfica
        var options = {'title':'',
                      'is3D':true,
                       'width':600,
                       'height':400};

        // Inicia la gráfica
        var chart = new google.visualization.PieChart(document.getElementById('grafica'));
        chart.draw(data, options);
      }
    </script>
	<style>
	#grafica{
		width:600px;
		margin:0 auto;
	}
	</style>
</head>
<body>
	<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
		<a href="inicio.php" class="navbar-brand">Gastos</a>
		<ul class="navbar-nav mr-auto mt-2 mt-lg-0">
			<li class="nav-item">
				<a href="inicio.php" class="nav-link">Resumen</a>
			</li>
			<li class="nav-item">
				<a href="categorias.php" class="nav-link">Categorías</a>
			</li>
			<li class="nav-item">
				<a href="cuentas.php" class="nav-link">Cuentas</a>
			</li>
			<li class="nav-item">
				<a href="movimientos.php" class="nav-link">Movimientos</a>
			</li>
			<li class="nav-item">
				<a href="traspasos.php" class="nav-link">Traspasos</a>
			</li>
			<li class="nav-item">
				<a href="presupuesto.php" class="nav-link active">Presupuesto</a>
			</li>
			<li class="nav-item">
				<a href="cxc.php" class="nav-link">CXC</a>
			</li>
			<li class="nav-item">
				<a href="admon.php" class="nav-link">Admon</a>
			</li>
		</ul>
		<ul class="navbar-nav">
			<li class="nav-item">
				<a href="salir.php" class="nav-link">Salir</a>
			</li>
		</ul>
	</nav>
	<div class="container-fluid text-center">
		<div class="row content">
			<div class="col-sm-2 sidevar">
				<label for="alta"></label>
				<input type="button" name="tabla" value="Tabla" class="btn btn-info mt-5" role="button" id="tabla">
			</div>
			<div class="col-sm-8 text-center">
				<h2>Presupuesto Gráfica</h2>
				<div id="grafica"></div>
			</div>
			<div class="col-sm-2 sidevar"></div>
		</div>
	</div>
</body>
</html>