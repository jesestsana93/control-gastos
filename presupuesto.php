<?php
require "php/variables.php";
require "php/funciones.php";
require "clases/Sesion.php";
require "clases/dbMySQL.php";
require "clases/Usuarios.php";
require "clases/Categorias.php";
require "clases/Movimientos.php";
/****************
Leemos la sesión
*****************/
$sesion = new Sesion();
$usuario = $sesion->getUsuario();
$data = Usuarios::leeUsuario($usuario);
$id = $data["id"]; //identificador del usuario
/*****************
Lee categorias
******************/
$categoria = new Categorias();
$categorias_array = array();
$numRegistros = $categoria->numRegistros($id);
if($numRegistros==0){
	array_push($msg,"1Error no hay categorías");
} else {
	$categorias_array = $categoria->leeCategoriasUsuario($id,0,$numRegistros);
}
/**********
Movimientos
**********/
$movimiento = new Movimientos();
/**********
Variables
**********/
$tot1 = 0;
$tot2 = 0;
$tot3 = 0;
$ejercido = 0;
$restante = 0;
/****************/
?>
<!DOCTYPE html>
<html>
<head>
	<title>Control de Gastos | Presupuesto</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1"/>
	<link rel="shortcut icon" href="imagenes/favicon.ico">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
	<script>
		window.onload = function(){
			document.getElementById("grafica").onclick = function() {
				window.open("presupuestoGrafica1.php","_self");
			}
		}
	</script>
	<style>
	button{ cursor:pointer; }
	.rojo{ color:red; }
	.azul{ color:blue; }
	</style>
</head>
<body>
	<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
		<a href="inicio.php" class="navbar-brand">Gastos</a>
		<ul class="navbar-nav mr-auto mt-2 mt-lg-0">
			<li class="nav-item">
				<a href="inicio.php" class="nav-link">Resumen</a>
			</li>
			<li class="nav-item">
				<a href="categorias.php" class="nav-link">Categorías</a>
			</li>
			<li class="nav-item">
				<a href="cuentas.php" class="nav-link">Cuentas</a>
			</li>
			<li class="nav-item">
				<a href="movimientos.php" class="nav-link">Movimientos</a>
			</li>
			<li class="nav-item">
				<a href="traspasos.php" class="nav-link">Traspasos</a>
			</li>
			<li class="nav-item">
				<a href="presupuesto.php" class="nav-link active">Presupuesto</a>
			</li>
			<li class="nav-item">
				<a href="cxc.php" class="nav-link">CXC</a>
			</li>
			<li class="nav-item">
				<a href="admon.php" class="nav-link">Admon</a>
			</li>
		</ul>
		<ul class="navbar-nav">
			<li class="nav-item">
				<a href="salir.php" class="nav-link">Salir</a>
			</li>
		</ul>
	</nav>
	<div class="container-fluid text-center">
		<div class="row content">
			<div class="col-sm-2 sidevar">
				<label for="alta"></label>
				<input type="button" name="grafica" value="Gráfica" class="btn btn-info mt-5" role="button" id="grafica">
			</div>
			<div class="col-sm-8 text-center">
				<h2>Presupuesto</h2>
				<?php require "php/mensajes.php";
				print "<table class='table table-striped' width='100%'>";
				print "<tr>";
				print "<th>Num. cuenta</th>";
				print "<th>Categoría</th>";
				print "<th>Tipo</th>";
				print "<th>Presupuesto</th>";
				print "<th>Ejercido</th>";
				print "<th>Restante</th>";
				print "</tr>";
				for ($i=0; $i < count($categorias_array); $i++) {
					if($categorias_array[$i]["tipo"]=="gasto"){
						$ejercido = $movimiento->montoCategoria($categorias_array[$i]["id"],$id);
						$restante = $categorias_array[$i]["presupuesto"] - $ejercido;
						$tot1 += $categorias_array[$i]["presupuesto"];
						$tot2 += $ejercido;
						$tot3 += $restante;
						print "<tr>";
						print "<td>".$categorias_array[$i]["id"]."</td>";
						print "<td>".$categorias_array[$i]["categoria"]."</td>";
						print "<td>".$categorias_array[$i]["tipo"]."</td>";
						print "<td>".number_format($categorias_array[$i]["presupuesto"],2)."</td>";
						print "<td>".number_format($ejercido,2)."</td>";
						if ($restante>=0) {
							print "<td class='azul'>";
						} else {
							print "<td class='rojo'>";
						}
						print number_format($restante,2)."</td>";
						print "</tr>";
					}
				}
				print "<tr>";
				print "<td>Totales:</td>";
				print "<td class='text-left'></td>";
				print "<td></td>";
				print "<td>".number_format($tot1,2)."</td>";
				print "<td>".number_format($tot2,2)."</td>";
				print "<td>".number_format($tot3,2)."</td>";
				print "</tr>";
				print "</table>";
				?>
			</div>
			<div class="col-sm-2 sidevar"></div>
		</div>
	</div>
</body>
</html>