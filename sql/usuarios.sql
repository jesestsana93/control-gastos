-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 21-07-2018 a las 18:11:31
-- Versión del servidor: 5.6.39-83.1
-- Versión de PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `pacoarce_escuela`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL,
  `usuario` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  `clave` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  `traspasos` int(11) NOT NULL,
  `cxc` int(11) NOT NULL,
  `ahorro` int(11) NOT NULL,
  `prestar` int(11) NOT NULL,
  `pagoPrestar` int(11) NOT NULL,
  `periodo` char(4) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `usuario`, `clave`, `traspasos`, `cxc`, `ahorro`, `prestar`, `pagoPrestar`, `periodo`) VALUES
(1, 'paco', 'paco', 25, 29, 0, 70, 71, '163'),
(10, 'fjarce2000@hotmail.com', '12345', 42, 43, 0, 0, 0, '1510'),
(11, 'fj_arce@yahoo.com.mx', '12345', 44, 45, 0, 0, 0, '1510'),
(12, 'gustavoilardo@gmail.com', 'CamilaRocio2005*', 48, 49, 0, 0, 0, '1510'),
(13, 'edorcu01@gmail.com', 'eocdis24', 50, 51, 0, 0, 0, '1510'),
(15, 'billy15ogt@gmail.com', 'acereros', 54, 55, 0, 0, 0, '1510'),
(16, 'monikqes@gmail.com', 'mapa2128', 56, 57, 0, 0, 0, '1510'),
(17, 'monigc.online@gmail.com', 'Chino369', 58, 59, 0, 0, 0, '1511'),
(18, 'doliver@tecnologia-web.com', '123456', 63, 64, 0, 0, 0, '1511'),
(19, 'fjarce2000@gmail.com', '12345', 66, 67, 0, 0, 0, '1512'),
(20, 'marloboro@gmail.com', 'maviagvavi', 74, 75, 0, 0, 0, '1601'),
(21, 'akin.ramirez.garcia2@gmail.com', 'Para2014', 80, 81, 0, 0, 0, '02'),
(22, 'internetciberclub@hotmail.com', 'quieroentrar', 82, 83, 0, 0, 0, '1602'),
(23, 'domiciliosjd@gmail.com', 'dim0627', 87, 88, 0, 0, 0, '163'),
(24, 'reyesmr@wanadoo.es', 'rey1973', 89, 90, 0, 0, 0, '1611'),
(25, 'fvicedo@hotmail.com', 'frvccr11045007', 92, 93, 0, 0, 0, '1701'),
(26, 'joelgomez_2006@hotmail,com', '63mz75gf', 94, 95, 0, 0, 0, '1701'),
(27, 'gustavolandeo@gmail.com', 'chulucanas3', 97, 98, 0, 0, 0, '1706'),
(28, 'carlos@gmail.com', 'entretuyyo', 99, 100, 0, 0, 0, '1707'),
(29, 'ateefd', '9921', 101, 102, 0, 0, 0, '1707'),
(30, 'a123', '123', 103, 104, 0, 0, 0, '07'),
(31, 'administrador123@gmail.com', '123456', 105, 106, 0, 0, 0, '1712'),
(32, 'pelotero289@gmail.com ', '8492087342r', 107, 108, 0, 0, 0, '12'),
(33, 'agarji1944@gmail.com', '1801', 109, 110, 0, 0, 0, '1802'),
(34, 'qfrogera@yahoo.es', 'raqf17071958', 113, 114, 0, 0, 0, '1803'),
(35, 'Angel', '123', 115, 116, 0, 0, 0, '1803'),
(36, 'alex3331992@hotmail.com', '123', 117, 118, 0, 0, 0, '03'),
(37, 'alex3331992@hotmail.com', '123', 117, 118, 0, 0, 0, '03'),
(38, 'alex3331992@hotmail.com', '123', 117, 118, 0, 0, 0, '03'),
(39, 'alex3331992@hotmail.com', '123', 117, 118, 0, 0, 0, '03'),
(40, 'alex3331992@hotmail.com', '123', 117, 118, 0, 0, 0, '03'),
(41, 'alex3331992@hotmail.com', '123', 117, 118, 0, 0, 0, '03'),
(42, 'alex3331992@hotmail.com', '123', 117, 118, 0, 0, 0, '03'),
(43, 'alex3331992@hotmail.com', '123', 117, 118, 0, 0, 0, '03'),
(44, 'alex3331992@hotmail.com', '123', 117, 118, 0, 0, 0, '03'),
(45, 'alex3331992@hotmail.com', '123', 117, 118, 0, 0, 0, '03'),
(46, 'alex3331992@hotmail.com', '123', 117, 118, 0, 0, 0, '03'),
(47, 'alex3331992@hotmail.com', '123', 117, 118, 0, 0, 0, '03'),
(48, 'alex3331992@hotmail.com', '123', 117, 118, 0, 0, 0, '03'),
(49, 'Angel', '123', 115, 116, 0, 0, 0, '1803'),
(50, 'alex3331992@hotmail.com', 'aaaa', 145, 146, 0, 0, 0, '03');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
