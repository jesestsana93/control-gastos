<?php
require "php/variables.php";
require "php/funciones.php";
require "clases/Sesion.php";
require "clases/dbMySQL.php";
require "clases/Usuarios.php";
require "clases/Cuentas.php";
/****************
Leemos la sesión
*****************/
$sesion = new Sesion();
$usuario = $sesion->getUsuario();
$data = Usuarios::leeUsuario($usuario);
$id = $data["id"]; //identificador del usuario
/*****************
Lee cuentas
******************/
$cuenta = new Cuentas();
$cuentas_array = array();
$numRegistros = $cuenta->numRegistros($id);
$cuentasTipos_array = $cuenta->leeCuentasTipos();
/******************/
require "php/paginaArriba.php";
/****************
Variables de trabajo
********************/
$idCuenta= "";
$nombreCuenta = "";
$saldo = 0;
$tipo = "";
/****************
Modo de la página (CRUD o ABC)
S - Consulta (select)
A - Alta (insert)
B - Borrar (delete)
C - Cambiar (update)
D - Baja Definitiva
*****************/
if (isset($_GET["m"])) {
	$m = $_GET["m"];
} else {
	$m = "S";
}
/**************
Validacion
**************/
if (isset($_POST["cuenta"])) {
	$idCuenta = (isset($_POST["idCuenta"]))?$_POST["idCuenta"]:"";
	$nombreCuenta = $_POST["cuenta"];
	$tipo = $_POST["tipo"];
	$saldo = $_POST["saldo"];
	$m="S";
	//validar
	if ($nombreCuenta=="") {
		array_push($msg,"1La cuenta no puede estar vacía");
	} else if($tipo=="0"){
		array_push($msg,"1Debes seleccionar un tipo de categoría");
	} else if($saldo<0){
		array_push($msg,"1El saldo no puede ser menor a cero");
	} else {
		$saldo = limpiaNumero($saldo);
		if($cuenta->altaCuenta($idCuenta,$id,"",$nombreCuenta,$tipo,$saldo)){
			array_push($msg,"0Alta exitosa");
		} else {
			array_push($msg,"1Error al insetar el registro");
		}
	}
}
//Baja definitiva
if($m=="D"){
	$idCuenta = $_GET["id"];
	$cuenta->borrarCuenta($idCuenta);
	$m = "S";
}
//Consulta o baja (previa) del registro
if($m=="C" || $m=="B"){
	$idCuenta = $_GET["id"];
	$data = $cuenta->leerRegistro($idCuenta);
	//
	$idCuenta = $data[0]["id"];
	$nombreCuenta = $data[0]["cuenta"];
	$tipo = $data[0]["tipo"];
	$saldo = $data[0]["saldo"];
} else if($m=="S"){
	$cuentas_array = $cuenta->leeCuentasUsuario($id,$inicio,$TAMANO_PAGINA);
}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Control de Gastos | Cuentas</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1"/>
	<link rel="shortcut icon" href="imagenes/favicon.ico">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
	<script>
		window.onload = function(){
			<?php if($m=="S"){ ?>
				document.getElementById("alta").onclick = function(){
					window.open("cuentas.php?m=A","_self");
				}
			<?php } 
			
			if($m=="C" || $m=="A"){ ?>
				document.getElementById("regresar").onclick = function(){
					window.open("cuentas.php","_self");
				}
			<?php } 
			
			if($m=="B"){ ?>
				document.getElementById("si").onclick = function(){
					var idCuenta = <?php print $idCuenta; ?>;
					window.open("cuentas.php?m=D&id="+idCuenta,"_self");
				}
				document.getElementById("no").onclick = function(){
					window.open("cuentas.php","_self");
				}
				document.getElementById("regresaBorrar").onclick = function(){
					window.open("cuentas.php","_self");
				}
			<?php } ?>
			
		}
		function cambiaPagina(p) {
			window.open("cuentas.php?p="+p,"_self");
		}
	</script>
	<style>
	button{ cursor:pointer; }
	.rojo{ color:red; }
	.verde{ color:green; }
	</style>
</head>
<body>
	<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
		<a href="inicio.php" class="navbar-brand">Gastos</a>
		<ul class="navbar-nav mr-auto mt-2 mt-lg-0">
			<li class="nav-item">
				<a href="inicio.php" class="nav-link">Resumen</a>
			</li>
			<li class="nav-item">
				<a href="categorias.php" class="nav-link">Categorías</a>
			</li>
			<li class="nav-item active">
				<a href="cuentas.php" class="nav-link">Cuentas</a>
			</li>
			<li class="nav-item">
				<a href="movimientos.php" class="nav-link">Movimientos</a>
			</li>
			<li class="nav-item">
				<a href="traspasos.php" class="nav-link">Traspasos</a>
			</li>
			<li class="nav-item">
				<a href="presupuesto.php" class="nav-link">Presupuesto</a>
			</li>
			<li class="nav-item">
				<a href="cxc.php" class="nav-link">CXC</a>
			</li>
			<li class="nav-item">
				<a href="admon.php" class="nav-link">Admon</a>
			</li>
		</ul>
		<ul class="navbar-nav">
			<li class="nav-item">
				<a href="salir.php" class="nav-link">Salir</a>
			</li>
		</ul>
	</nav>
	<div class="container-fluid text-center">
		<div class="row content">
			<div class="col-sm-2 sidevar">
				<?php if ($m=="S") { ?>
					<label for="alta"></label>
					<input type="button" name="alta" value="Dar de alta una cuenta" class="btn btn-info mt-5" role="button" id="alta">
				<?php } ?>
			</div>
			<div class="col-sm-8 text-center">
				<h2>Cuentas</h2>
				<?php if($m=="C" || $m=="A" || $m=="B") { 
					require "php/mensajes.php";
				?>
					<form action="cuentas.php" method="post">
						<div class="form-group text-left">
							<label for="cuenta">* Cuenta:</label>
							<input type="text" name="cuenta" id="cuenta" required class="form-control" placeholder="Escribe el nombre de la cuenta" value="<?php print $nombreCuenta;?>" <?php print ($m=='B')?'disabled':""; ?> />
						</div>
						<div class="form-group text-left">
							<label for="tipo">* Tipo de cuenta:</label><br>
							<select id="tipo" name="tipo" class="form-control" <?php print ($m=='B')?'disabled':""; ?>>
								<option value="0">Selecciona un tipo de cuenta</option>
								<?php
								for ($i=0; $i < count($cuentasTipos_array); $i++) { 
									print "<option ";
									print ($cuentasTipos_array[$i]["cuenta"]==$tipo)?"selected":"";
									print " value='".$cuentasTipos_array[$i]["cuenta"]."'>";
									print $cuentasTipos_array[$i]["cuenta"];
									print "</option>";
								}
								?>
							</select>
						</div>
						<div class="form-group text-left">
							<label for="saldo">Saldo:</label>
							<input type="texto" name="saldo" id="saldo" class="form-control" placeholder="Escribe el saldo inicial de la cuenta" value="<?php print number_format($saldo,2); ?>" <?php print ($m=='B')?'disabled':""; ?>/>
						</div>
						<input type="hidden" id="idCuenta" name="idCuenta" value="<?php print $idCuenta; ?>">
						<?php if($m=="C" || $m=="A") { ?>
						<div class="form-group text-left">
							<label for="enviar"></label>
							<input type="submit" name="enviar" id="enviar" class="btn btn-success" value="Enviar datos"/>

							<label for="regresar"></label>
							<input type="button" name="regresar" id="regresar" class="btn btn-info" value="Regresar" role="button"/>
						</div>
						<?php } else if($m=="B"){
							if($cuenta->sePuedeBorrar($idCuenta)){?>
								<div class="alert alert-danger">
									<p><b>Advertencia:</b> Una vez borrado el registro, no se podrá recuperar.</p>
									<p>¿Desea borrar el registro?</p>
									<label for="si"></label>
									<input type="button" name="si" id="si" class="btn btn-danger" value="Si"/>

									<label for="No"></label>
									<input type="button" name="no" id="no" class="btn" value="No" role="button"/>
									<input type="hidden" id="regresaBorrar" name="regresaBorrar">
								</div>
						<?php } else {?>
								<div class="alert alert-danger">
									<p><b>Advertencia:</b> esta cuenta tiene movimientos y no se puede borrar.</p>
									<label for="regresaBorrar"></label>
									<input type="button" name="regresaBorrar" id="regresaBorrar" class="btn btn-danger" value="Regresa"/>
									<input type="hidden" id="si" name="si">
									<input type="hidden" id="no" name="no">
								</div>
						<?php }
					} ?>
					</form>
				<?php
				}
				if($m=="S"){
					print "<table class='table table-striped' width='100%'>";
					print "<tr>";
					print "<th>id</th>";
					print "<th>Cuentas</th>";
					print "<th>Tipo</th>";
					print "<th>Saldo inicial</th>";
					print "<th>Cargos</th>";
					print "<th>Abonos</th>";
					print "<th>Saldo final</th>";
					print "<th>Modificar</th>";
					print "<th>Borrar</th>";
					print "</tr>";
					for ($i=0; $i < count($cuentas_array); $i++) { 
						$saldo = $cuentas_array[$i]["saldo"] - $cuentas_array[$i]["cargos"] + $cuentas_array[$i]["abonos"]; 
						print "<tr>";
						print "<td>".$cuentas_array[$i]["id"]."</td>";
						print "<td>".$cuentas_array[$i]["cuenta"]."</td>";
						print "<td>".$cuentas_array[$i]["tipo"]."</td>";
						print "<td>".number_format($cuentas_array[$i]["saldo"],2)."</td>";
						print "<td>".number_format($cuentas_array[$i]["cargos"],2)."</td>";
						print "<td>".number_format($cuentas_array[$i]["abonos"],2)."</td>";
						if($cuentas_array[$i]["tipo"]=="Credito"){
							print "<td class='rojo'>(".number_format(abs($saldo),2).")</td>";
						} else {
							print "<td class='verde'>".number_format($saldo,2)."</td>";
						}
						print "<td><a class='btn btn-info' href='cuentas.php?m=C&id=".$cuentas_array[$i]["id"]."'>Modificar</a></td>";
						print "<td><a class='btn btn-danger' href='cuentas.php?m=B&id=".$cuentas_array[$i]["id"]."'>Borrar</a></td>";
						print "</tr>";
					}
					print "</table>";
					require "php/paginaBaja.php";
				}
				?>
			</div>
			<div class="col-sm-2 sidevar"></div>
		</div>
	</div>
</body>
</html>