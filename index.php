<?php
require "php/variables.php";
require "clases/Sesion.php";
require "clases/dbMySQL.php";
require "clases/Usuarios.php";
/****************
Leemos la sesión
*****************/
$sesion = new Sesion();
/*********************
Validación del usuario
**********************/
if (isset($_POST["usuario"])) {
	$usuario = $_POST["usuario"];
	$clave = $_POST["clave"];
	$clave = substr(hash_hmac("sha512",$clave,"mimamamemima"),0,100);
	//
	if (Usuarios::buscaUsuario($usuario, $clave)) {
		$sesion->inicioLogin($usuario);
		header("location:inicio.php");
		exit;
	} else {
		array_push($msg, "1Clave de acceso o usuario inválidos");
	}
}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Control de Gastos</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1"/>
	<link rel="shortcut icon" href="imagenes/favicon.ico">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
</head>
<body>
	<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
		<a href="index.php" class="navbar-brand">Gastos</a>
	</nav>
	<div class="container-fluid text-center">
		<div class="row content">
			<div class="col-sm-2 sidevar"></div>
			<div class="col-sm-8 text-center">
				<h2>Iniciar sesión</h2>
				<?php require "php/mensajes.php"; ?>
				<form class="text-left" action="index.php" method="post">
					<div class="form-group">
						<label for="usuario">Usuario:</label>
						<input type="text" name="usuario" id="usuario" class="form-control" required placeholder="Escribe tu usuario"/>
					</div>
					<div class="form-group">
						<label for="clave">Clave de acceso:</label>
						<input type="password" name="clave" id="clave" class="form-control" required placeholder="Escribe tu clave de acceso"/>
					</div>
					<div class="form-group">
						<label for="entrar"></label>
						<input type="submit" name="entrar" id="entrar" class="btn btn-success" role="button" value="Entrar" />
					</div>
				</form>
			</div>
			<div class="col-sm-2 sidevar"></div>
		</div>
	</div>
</body>
</html>