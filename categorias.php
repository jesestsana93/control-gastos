<?php
require "php/variables.php";
require "php/funciones.php";
require "clases/Sesion.php";
require "clases/dbMySQL.php";
require "clases/Usuarios.php";
require "clases/Categorias.php";
/****************
Leemos la sesión
*****************/
$sesion = new Sesion();
$usuario = $sesion->getUsuario();
$data = Usuarios::leeUsuario($usuario);
$id = $data["id"]; //identificador del usuario
/*****************
Lee categorias
******************/
$categoria = new Categorias();
$categorias_array = array();
$numRegistros = $categoria->numRegistros($id);
if($numRegistros==0){
	if($categoria->crearCategoriasBasicas($id)){
		$categorias_array = $categoria->leeCategoriasUsuario($id);
	} else {
		array_push($msg,"1Error al crear las categorías");
	}
}
$categoriasTipos_array = $categoria->leeCategoriasTipos();
/******************/
require "php/paginaArriba.php";
/****************
Variables de trabajo
********************/
$idCategoria= "";
$nombreCategoria = "";
$tipo = "";
$nota = "";
$presupuesto=0;
/****************
Modo de la página (CRUD o ABC)
S - Consulta (select)
A - Alta (insert)
B - Borrar (delete)
C - Cambiar (update)
D - Baja Definitiva
*****************/
if (isset($_GET["m"])) {
	$m = $_GET["m"];
} else {
	$m = "S";
}
/**************
Validacion
**************/
if (isset($_POST["categoria"])) {
	$idCategoria = (isset($_POST["idCategoria"]))?$_POST["idCategoria"]:"";
	$nombreCategoria = $_POST["categoria"];
	$tipo = $_POST["tipo"];
	$presupuesto = $_POST["presupuesto"];
	$nota = $_POST["nota"];
	$m="S";
	//validar
	if ($nombreCategoria=="") {
		array_push($msg,"1La categoria no puede estar vacía");
	} else if($tipo=="0"){
		array_push($msg,"1Debes seleccionar un tipo de categoría");
	} else if($presupuesto<0){
		array_push($msg,"1El presupuesto no puede ser menor a cero");
	} else {
		$presupuesto = limpiaNumero($presupuesto);
		if($categoria->altaCategoria($idCategoria,$id,$nombreCategoria,$tipo,$presupuesto,$nota)){
			array_push($msg,"0Alta exitosa");
		} else {
			array_push($msg,"1Error al insetar el registro");
		}
	}
}
//Baja definitiva
if($m=="D"){
	$idCategoria = $_GET["id"];
	$categoria->borrarCategoria($idCategoria);
	$m = "S";
}
//Consulta o baja (previa) del registro
if($m=="C" || $m=="B"){
	$idCategoria = $_GET["id"];
	$data = $categoria->leerRegistro($idCategoria);
	//
	$idCategoria = $data[0]["id"];
	$nombreCategoria = $data[0]["categoria"];
	$tipo = $data[0]["tipo"];
	$nota = $data[0]["nota"];
	$presupuesto = $data[0]["presupuesto"];
} else if($m=="S"){
	$categorias_array = $categoria->leeCategoriasUsuario($id,$inicio,$TAMANO_PAGINA);
}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Control de Gastos | Categorias</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1"/>
	<link rel="shortcut icon" href="imagenes/favicon.ico">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
	<script>
		window.onload = function(){
			<?php if($m=="S"){ ?>
				document.getElementById("alta").onclick = function(){
					window.open("categorias.php?m=A","_self");
				}
			<?php } 
			
			if($m=="C" || $m=="A"){ ?>
				document.getElementById("regresar").onclick = function(){
					window.open("categorias.php","_self");
				}
			<?php } 
			
			if($m=="B"){ ?>
				document.getElementById("si").onclick = function(){
					var idCategoria = <?php print $idCategoria; ?>;
					window.open("categorias.php?m=D&id="+idCategoria,"_self");
				}
				document.getElementById("no").onclick = function(){
					window.open("categorias.php","_self");
				}
				document.getElementById("regresaBorrar").onclick = function(){
					window.open("categorias.php","_self");
				}
			<?php } ?>
			
		}
		function cambiaPagina(p) {
			window.open("categorias.php?p="+p,"_self");
		}
	</script>
	<style>
	button{ cursor:pointer; }
	</style>
</head>
<body>
	<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
		<a href="inicio.php" class="navbar-brand">Gastos</a>
		<ul class="navbar-nav mr-auto mt-2 mt-lg-0">
			<li class="nav-item">
				<a href="inicio.php" class="nav-link">Resumen</a>
			</li>
			<li class="nav-item">
				<a href="categorias.php" class="nav-link active">Categorías</a>
			</li>
			<li class="nav-item">
				<a href="cuentas.php" class="nav-link">Cuentas</a>
			</li>
			<li class="nav-item">
				<a href="movimientos.php" class="nav-link">Movimientos</a>
			</li>
			<li class="nav-item">
				<a href="traspasos.php" class="nav-link">Traspasos</a>
			</li>
			<li class="nav-item">
				<a href="presupuesto.php" class="nav-link">Presupuesto</a>
			</li>
			<li class="nav-item">
				<a href="cxc.php" class="nav-link">CXC</a>
			</li>
			<li class="nav-item">
				<a href="admon.php" class="nav-link">Admon</a>
			</li>
		</ul>
		<ul class="navbar-nav">
			<li class="nav-item">
				<a href="salir.php" class="nav-link">Salir</a>
			</li>
		</ul>
	</nav>
	<div class="container-fluid text-center">
		<div class="row content">
			<div class="col-sm-2 sidevar">
				<?php if ($m=="S") { ?>
					<label for="alta"></label>
					<input type="button" name="alta" value="Añadir una categoría" class="btn btn-info mt-5" role="button" id="alta">
				<?php } ?>
			</div>
			<div class="col-sm-8 text-center">
				<h2>Categorías</h2>
				<?php if($m=="C" || $m=="A" || $m=="B") { 
					require "php/mensajes.php";
				?>
					<form action="categorias.php" method="post">
						<div class="form-group text-left">
							<label for="categoria">* Categoría:</label>
							<input type="text" name="categoria" id="categoria" required class="form-control" placeholder="Escribe el nombre de la categoría" value="<?php print $nombreCategoria;?>" <?php print ($m=='B')?'disabled':""; ?> />
						</div>
						<div class="form-group text-left">
							<label for="tipo">* Tipo de categoría:</label><br>
							<select id="tipo" name="tipo" class="form-control" <?php print ($m=='B')?'disabled':""; ?>>
								<option value="0">Selecciona un tipo de categoría</option>
								<?php
								for ($i=0; $i < count($categoriasTipos_array); $i++) { 
									print "<option ";
									print ($categoriasTipos_array[$i]["tipo"]==$tipo)?"selected":"";
									print " value='".$categoriasTipos_array[$i]["tipo"]."'>";
									print $categoriasTipos_array[$i]["tipo"];
									print "</option>";
								}
								?>
							</select>
						</div>
						<div class="form-group text-left">
							<label for="presupuesto">Presupuesto:</label>
							<input type="texto" name="presupuesto" id="presupuesto" class="form-control" placeholder="Escribe tu presupuesto para esta categoría" value="<?php print number_format($presupuesto,2); ?>" <?php print ($m=='B')?'disabled':""; ?>/>
						</div>
						<div class="form-group text-left">
							<label for="nota">Nota:</label><br>
							<textarea id="nota" name="nota" <?php print ($m=='B')?'disabled':""; ?> class="form-control"><?php print $nota; ?></textarea>
						</div>
						<input type="hidden" id="idCategoria" name="idCategoria" value="<?php print $idCategoria; ?>">
						<?php if($m=="C" || $m=="A") { ?>
						<div class="form-group text-left">
							<label for="enviar"></label>
							<input type="submit" name="enviar" id="enviar" class="btn btn-success" value="Enviar datos"/>

							<label for="regresar"></label>
							<input type="button" name="regresar" id="regresar" class="btn btn-info" value="Regresar" role="button"/>
						</div>
						<?php } else if($m=="B"){
							if($categoria->sePuedeBorrar($idCategoria)){ ?>
								<div class="alert alert-danger">
									<p><b>Advertencia:</b> Una vez borrado el registro, no se podrá recuperar.</p>
									<p>¿Desea borrar el registro?</p>
									<label for="si"></label>
									<input type="button" name="si" id="si" class="btn btn-danger" value="Si"/>

									<label for="No"></label>
									<input type="button" name="no" id="no" class="btn" value="No" role="button"/>
									<input type="hidden" name="regresaBorrar" id="regresaBorrar"/>
								</div>
						<?php } else { ?>
							<div class="alert alert-danger">
									<p><b>Advertencia:</b> Esta categoría tiene movimientos, no la puedes eliminar.</p>
									<label for="regresaBorrar"></label>
									<input type="button" name="regresaBorrar" id="regresaBorrar" class="btn btn-danger" value="Regresar"/>
									<input type="hidden" name="no" id="no"/>
									<input type="hidden" name="si" id="si"/>
							</div>
						<?php } 
					}?>
					</form>
				<?php
				}
				if($m=="S"){
					print "<table class='table table-striped' width='100%'>";
					print "<tr>";
					print "<th>id</th>";
					print "<th>Categoría</th>";
					print "<th>Tipo</th>";
					print "<th>Presupuesto</th>";
					print "<th>Modificar</th>";
					print "<th>Borrar</th>";
					print "</tr>";
					for ($i=0; $i < count($categorias_array); $i++) { 
						print "<tr>";
						print "<td>".$categorias_array[$i]["id"]."</td>";
						print "<td>".$categorias_array[$i]["categoria"]."</td>";
						print "<td>".$categorias_array[$i]["tipo"]."</td>";
						print "<td>".number_format($categorias_array[$i]["presupuesto"],2)."</td>";
						print "<td><a class='btn btn-warning' href='categorias.php?m=C&id=".$categorias_array[$i]["id"]."'>Modificar</a></td>";
						print "<td><a class='btn btn-danger' href='categorias.php?m=B&id=".$categorias_array[$i]["id"]."'>Borrar</a></td>";
						print "</tr>";
					}
					print "</table>";
					require "php/paginaBaja.php";
				}
				?>
			</div>
			<div class="col-sm-2 sidevar"></div>
		</div>
	</div>
</body>
</html>