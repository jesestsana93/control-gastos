<?php
require "php/variables.php";
require "php/funciones.php";
require "clases/Sesion.php";
require "clases/dbMySQL.php";
require "clases/Usuarios.php";
require "clases/Cuentas.php";
require "clases/Categorias.php";
require "clases/Movimientos.php";
require "clases/Traspasos.php";
/****************
Leemos la sesión
*****************/
$sesion = new Sesion();
$usuario = $sesion->getUsuario();
$data = Usuarios::leeUsuario($usuario);
$id = $data["id"]; //identificador del usuario
/*****************
Lee cuentas
******************/
$cuenta = new Cuentas();
$cuentas_array = array();
$cuentas_array = $cuenta->leeCuentasUsuario($id);
$cuentasTipos_array = $cuenta->leeCuentasTipos();
/******************
Categorias
*******************/
$categoria = new Categorias();
$categoriaTraspaso = $categoria->categoriaTraspaso($id);
/******************
Movimientos
*******************/
$movimiento = new Movimientos();
/******************
Traspasos
*******************/
$traspaso = new Traspasos();
$traspasos_array = $traspaso->leeTraspasosUsuario($id);
$numRegistros = $traspaso->numRegistros($id);
/******************/
require "php/paginaArriba.php";
/****************
Variables de trabajo
********************/
$idCuenta= "";
$origen = "";
$destino = "";
$monto = 0;
$fecha = "";
$nota = "";
/****************
Modo de la página (CRUD o ABC)
S - Consulta (select)
A - Alta (insert)
B - Borrar (delete)
C - Cambiar (update)
D - Baja Definitiva
*****************/
if (isset($_GET["m"])) {
	$m = $_GET["m"];
} else {
	$m = "S";
}
/**************
Validacion
**************/
if (isset($_POST["origen"])) {
	$origen = (isset($_POST["origen"]))?$_POST["origen"]:NULL;
	$destino = (isset($_POST["destino"]))?$_POST["destino"]:NULL;
	$monto = $_POST["monto"];
	$fecha = $_POST["fecha"];
	$nota = $_POST["nota"];
	$monto = limpiaNumero($monto);
	//
	//Recuperamos el saldo
	//
	for ($i=0; $i < count($cuentas_array) ; $i++) { 
		if ($cuentas_array[$i]["id"]==$origen) {
			$saldo = $cuentas_array[$i]["saldo"]-$cuentas_array[$i]["cargos"]+$cuentas_array[$i]["abonos"];
			break;
		} 
	}
	//validar
	if ($origen=="") {
		array_push($msg,"1La cuenta origen no puede estar vacía");
	} else if($destino==""){
		array_push($msg,"1La cuenta destino no puede estar vacía");
	} else if($monto<=0){
		array_push($msg,"1El monto no puede ser menor o igual a cero");
	} else if($origen==$destino){
		array_push($msg,"1La cuenta origen debe ser diferente a la cuenta de destino");
	} else if(!validaFecha($fecha)){
		array_push($msg,"1La cuenta origen debe ser diferente a la cuenta de destino");
	} else if(!validaDecimal($monto)){
		array_push($msg,"1El monto solo acepta valores numéricos");
	} else if($monto>$saldo){
		array_push($msg,"1El saldo en la cuenta de origen es insuficiente");
	} else {
		//
		//movimiento de "gasto" (Cargo)
		//
		$movimiento->altaMovimiento("",$id,"","gasto", $categoriaTraspaso, $origen, $monto, $fecha,$nota);
		$cuenta->actualizaSaldo($origen,"gasto",$monto);
		//
		//Movimiento de "ingreso (Abono)"
		//
		$movimiento->altaMovimiento("",$id,"","ingreso", $categoriaTraspaso, $destino, $monto, $fecha,$nota);
		$cuenta->actualizaSaldo($destino,"ingreso",$monto);
		//
		//Registro de traspaso
		//
		$traspaso->altaRegistro("",$id,$origen,$destino,$monto,$fecha,$nota);
	}
	$m="A";
}
//Baja definitiva
if($m=="D"){
	$idCuenta = $_GET["id"];
	$cuenta->borrarCuenta($idCuenta);
	$m = "S";
}
//Consulta o baja (previa) del registro
if($m=="C" || $m=="B"){
	$idCuenta = $_GET["id"];
	$data = $cuenta->leerRegistro($idCuenta);
	//
	$idCuenta = $data[0]["id"];
	$nombreCuenta = $data[0]["cuenta"];
	$tipo = $data[0]["tipo"];
	$saldo = $data[0]["saldo"];
} else if($m=="S"){
	$cuentas_array = $cuenta->leeCuentasUsuario($id,$inicio,$TAMANO_PAGINA);
}
function nombreCuenta($cuenta, $cuentas_array){
	$nombre = "";
	for ($i=0; $i < count($cuentas_array); $i++) { 
		if ($cuenta==$cuentas_array[$i]["id"]) {
			$nombre = $cuentas_array[$i]["cuenta"];
			break;
		}
	}
	return $nombre;
}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Control de Gastos | Traspaso entre cuentas</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1"/>
	<link rel="shortcut icon" href="imagenes/favicon.ico">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
	<script>
		window.onload = function(){
			<?php if($m=="S"){ ?>
				document.getElementById("alta").onclick = function(){
					window.open("traspasos.php?m=A","_self");
				}
			<?php } 
			
			if($m=="C" || $m=="A"){ ?>
				document.getElementById("regresar").onclick = function(){
					window.open("traspasos.php","_self");
				}
			<?php } 
			
			if($m=="B"){ ?>
				document.getElementById("si").onclick = function(){
					var idCuenta = <?php print $idCuenta; ?>;
					window.open("traspasos.php?m=D&id="+idCuenta,"_self");
				}
				document.getElementById("no").onclick = function(){
					window.open("traspasos.php","_self");
				}
			<?php } ?>
			
		}
		function cambiaPagina(p) {
			window.open("traspasos.php?p="+p,"_self");
		}
	</script>
	<style>
	button{ cursor:pointer; }
	</style>
</head>
<body>
	<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
		<a href="inicio.php" class="navbar-brand">Gastos</a>
		<ul class="navbar-nav mr-auto mt-2 mt-lg-0">
			<li class="nav-item">
				<a href="inicio.php" class="nav-link">Resumen</a>
			</li>
			<li class="nav-item">
				<a href="categorias.php" class="nav-link">Categorías</a>
			</li>
			<li class="nav-item">
				<a href="cuentas.php" class="nav-link">Cuentas</a>
			</li>
			<li class="nav-item">
				<a href="movimientos.php" class="nav-link">Movimientos</a>
			</li>
			<li class="nav-item active">
				<a href="traspasos.php" class="nav-link">Traspasos</a>
			</li>
			<li class="nav-item">
				<a href="presupuesto.php" class="nav-link">Presupuesto</a>
			</li>
			<li class="nav-item">
				<a href="cxc.php" class="nav-link">CXC</a>
			</li>
			<li class="nav-item">
				<a href="admon.php" class="nav-link">Admon</a>
			</li>
		</ul>
		<ul class="navbar-nav">
			<li class="nav-item">
				<a href="salir.php" class="nav-link">Salir</a>
			</li>
		</ul>
	</nav>
	<div class="container-fluid text-center">
		<div class="row content">
			<div class="col-sm-2 sidevar">
				<?php if ($m=="S") { ?>
					<label for="alta"></label>
					<input type="button" name="alta" value="Dar de alta un traspaso" class="btn btn-info mt-5" role="button" id="alta">
				<?php } ?>
			</div>
			<div class="col-sm-8 text-center">
				<h2>Traspasos</h2>
				<?php if($m=="C" || $m=="A" || $m=="B") { 
					require "php/mensajes.php";
				?>
					<form action="traspasos.php" method="post">
						<div class="form-group text-left">
							<label for="origen">* Cuenta origen:</label>
							<select id="origen" name="origen" class="form-control" <?php print ($m=='B')?'disabled':""; ?>>
								<option value="">Selecciona la cuenta origen:</option>
								<?php
								for ($i=0; $i < count($cuentas_array); $i++) { 
									$saldo = $cuentas_array[$i]["saldo"]-$cuentas_array[$i]["cargos"]+$cuentas_array[$i]["abonos"];
									print "<option ";
									print " value='".$cuentas_array[$i]["id"]."'>";
									print $cuentas_array[$i]["cuenta"];
									print " ($".number_format($saldo,2).")";
									print "</option>";
								}
								?>
							</select>
						</div>
						<div class="form-group text-left">
							<label for="destino">* Cuenta destino:</label><br>
							<select id="destino" name="destino" class="form-control" <?php print ($m=='B')?'disabled':""; ?>>
								<option value="">Selecciona la cuenta destino</option>
								<?php
								for ($i=0; $i < count($cuentas_array); $i++) {
									$saldo = $cuentas_array[$i]["saldo"]-$cuentas_array[$i]["cargos"]+$cuentas_array[$i]["abonos"];
									print "<option ";
									print " value='".$cuentas_array[$i]["id"]."'>";
									print $cuentas_array[$i]["cuenta"];
									print " ($".number_format($saldo,2).")";
									print "</option>";
								}
								?>
							</select>
						</div>
						<div class="form-group text-left">
							<label for="monto">Monto:</label>
							<input type="texto" name="monto" id="monto" class="form-control" placeholder="Escribe el monto del traspaso" value="<?php print number_format(0,2); ?>" <?php print ($m=='B')?'disabled':""; ?>/>
						</div>
						<div class="form-group text-left">
							<label for="fecha">* Fecha:</label>
							<input type="date" name="fecha" id="fecha" required class="form-control" placeholder="AAAA-MM-DD" value="<?php print $fecha; ?>" <?php print ($m=='B')?'disabled':""; ?>/>
						</div>
						<div class="form-group text-left">
							<label for="nota">Nota:</label>
							<input type="text" name="nota" id="nota" class="form-control" placeholder="Escribe una nota" value="<?php print $nota; ?>" <?php print ($m=='B')?'disabled':""; ?>/>
						</div>
						<?php if($m=="C" || $m=="A") { ?>
						<div class="form-group text-left">
							<label for="enviar"></label>
							<input type="submit" name="enviar" id="enviar" class="btn btn-success" value="Enviar datos"/>

							<label for="regresar"></label>
							<input type="button" name="regresar" id="regresar" class="btn btn-info" value="Regresar" role="button"/>
						</div>
						<?php } else if($m=="B"){?>
						<div class="alert alert-danger">
							<p><b>Advertencia:</b> Una vez borrado el registro, no se podrá recuperar.</p>
							<p>¿Desea borrar el registro?</p>
							<label for="si"></label>
							<input type="button" name="si" id="si" class="btn btn-danger" value="Si"/>

							<label for="No"></label>
							<input type="button" name="no" id="no" class="btn" value="No" role="button"/>
						</div>
						<?php } ?>
					</form>
				<?php
				}
				if($m=="S"){
					print "<table class='table table-striped' width='100%'>";
					print "<tr>";
					print "<th>id</th>";
					print "<th>Cuenta origen</th>";
					print "<th>Cuenta destino</th>";
					print "<th>Monto</th>";
					print "<th>Fecha</th>";
					print "<th>Nota</th>";
					print "</tr>";
					//
					for ($i=0; $i < count($traspasos_array); $i++) { 
						print "<tr>";
						print "<td>".$traspasos_array[$i]["id"]."</td>";
						print "<td class='text-left'>";
						print nombreCuenta($traspasos_array[$i]["origen"], $cuentas_array);
						print "</td>";
						print "<td class='text-left'>";
						print nombreCuenta($traspasos_array[$i]["destino"], $cuentas_array);
						print "</td>";
						print "<td class='text-right'>".number_format($traspasos_array[$i]["monto"],2)."</td>";
						print "<td>".$traspasos_array[$i]["fecha"]."</td>";
						print "<td class='text-left'>".$traspasos_array[$i]["nota"]."</td>";
						print "</tr>";
					}
					//
					print "</table>";
					require "php/paginaBaja.php";
				}
				?>
			</div>
			<div class="col-sm-2 sidevar"></div>
		</div>
	</div>
</body>
</html>